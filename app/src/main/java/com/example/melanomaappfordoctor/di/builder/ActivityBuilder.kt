package com.example.melanomaappfordoctor.di.builder

import com.example.melanomaappfordoctor.ui.detail_periksa.DetailPeriksaActivityModule
import com.example.melanomaappfordoctor.ui.detail_periksa.view.DetailPeriksaActivity
import com.example.melanomaappfordoctor.ui.edit_periksa.EditPeriksaActivityModule
import com.example.melanomaappfordoctor.ui.edit_periksa.view.EditPeriksaActivity
import com.example.melanomaappfordoctor.ui.edit_profile.EditProfileActivityModule
import com.example.melanomaappfordoctor.ui.edit_profile.view.EditProfileActivity
import com.example.melanomaappfordoctor.ui.login.LoginModuleActivity
import com.example.melanomaappfordoctor.ui.login.view.LoginActivity
import com.example.melanomaappfordoctor.ui.main.MainActivityModule
import com.example.melanomaappfordoctor.ui.main.view.MainActivity
import com.example.melanomaappfordoctor.ui.main.view.fragments.FragmentProvider
import com.example.melanomaappfordoctor.ui.register.RegisterActivityModule
import com.example.melanomaappfordoctor.ui.register.view.RegisterActivity
import com.example.melanomaappfordoctor.ui.splashscreen.SplashscreenActivityModule
import com.example.melanomaappfordoctor.ui.splashscreen.view.SplashscreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [(MainActivityModule::class), (FragmentProvider::class)])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(LoginModuleActivity::class)])
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(SplashscreenActivityModule::class)])
    abstract fun bindSplashscreenActivity(): SplashscreenActivity

    @ContributesAndroidInjector(modules = [(DetailPeriksaActivityModule::class)])
    abstract fun bindDetailPeriksaActivity(): DetailPeriksaActivity

    @ContributesAndroidInjector(modules = [(EditPeriksaActivityModule::class)])
    abstract fun bindEditPeriksaActivity(): EditPeriksaActivity

    @ContributesAndroidInjector(modules = [(RegisterActivityModule::class)])
    abstract fun bindRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector(modules = [(EditProfileActivityModule::class)])
    abstract fun bindEditProfileActivity(): EditProfileActivity
}