package com.example.melanomaappfordoctor.di.component

import android.app.Application
import com.example.melanomaappfordoctor.MelanomaApp
import com.example.melanomaappfordoctor.di.builder.ActivityBuilder
import com.example.melanomaappfordoctor.di.module.AppModule
import com.example.melanomaappfordoctor.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import org.jetbrains.anko.Android
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),
        (ActivityBuilder::class),
        (NetworkModule::class),
        (AppModule::class)]
)

interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: MelanomaApp)
}