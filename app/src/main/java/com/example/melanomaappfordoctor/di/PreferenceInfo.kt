package com.example.melanomaappfordoctor.di

import javax.inject.Qualifier

@Qualifier
@Retention annotation class PreferenceInfo