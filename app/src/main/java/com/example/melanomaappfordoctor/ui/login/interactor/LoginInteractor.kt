package com.example.melanomaappfordoctor.ui.login.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.request.LoginRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.LoginResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.di.PreferenceInfo
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class LoginInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
): BaseInteractor(preferencesHelper, apiHelper), LoginMvpInteractor{
    override fun login(loginRequest: LoginRequest): Observable<BaseResponse<LoginResponse>> = apiHelper.login(loginRequest)

    override fun saveToken(token: String) {
        preferencesHelper.setToken(token)
    }

    override fun setId(id: Int) {
        preferencesHelper.setIdUser(id)
    }

}