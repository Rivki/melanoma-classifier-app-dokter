package com.example.melanomaappfordoctor.ui.main.view.fragments.history.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.data.network.response.HistoryPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.view.BaseFragment
import com.example.melanomaappfordoctor.ui.detail_periksa.view.DetailPeriksaActivity
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor.HistoryPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.presenter.HistoryPeriksaMvpPresenter
import kotlinx.android.synthetic.main.fragment_history.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

class HistoryPeriksaFragment: BaseFragment(), HistoryPeriksaMvpView {

    @Inject
    lateinit var presenter: HistoryPeriksaMvpPresenter<HistoryPeriksaMvpView, HistoryPeriksaMvpInteractor>

    private lateinit var rvAdapter: HistoryPeriksaAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    companion object{
        @JvmStatic
        fun newInstance(): HistoryPeriksaFragment{
            return HistoryPeriksaFragment().apply {
                arguments = Bundle()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun setUp() {
        presenter.let {
            it.onAttach(this)
            it.getDataHistory()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getDataHistory()
    }

    override fun loadDataHistory(data: HistoryPeriksaResponse) {
        if(data.data.isEmpty()){
            data_empty.visibility = View.VISIBLE
            rv_history.visibility = View.GONE
        }else{
            data_empty.visibility = View.GONE
            rv_history.visibility = View.VISIBLE
            val layoutManagers = LinearLayoutManager(activity)
            rv_history.apply {
                layoutManager = layoutManagers
                rvAdapter = HistoryPeriksaAdapter(data.data){
                    startActivity<DetailPeriksaActivity>(
                        DetailPeriksaActivity.ID_PERIKSA to it.id,
                        DetailPeriksaActivity.STATUS to it.status
                    )
                }
                adapter = rvAdapter
            }
        }
    }
}