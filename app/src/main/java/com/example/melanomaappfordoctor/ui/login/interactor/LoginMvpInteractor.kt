package com.example.melanomaappfordoctor.ui.login.interactor

import com.example.melanomaappfordoctor.data.network.request.LoginRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.LoginResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface LoginMvpInteractor : MvpInteractor {
    fun login(loginRequest: LoginRequest): Observable<BaseResponse<LoginResponse>>
    fun saveToken(token: String)
    fun setId(id: Int)
}