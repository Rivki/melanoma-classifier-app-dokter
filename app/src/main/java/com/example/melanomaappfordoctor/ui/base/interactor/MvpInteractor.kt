package com.example.melanomaappfordoctor.ui.base.interactor

interface MvpInteractor {
    fun isUserLoggedIn(): Boolean
    fun performUserLogout()
}