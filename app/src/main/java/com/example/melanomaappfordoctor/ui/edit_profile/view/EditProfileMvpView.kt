package com.example.melanomaappfordoctor.ui.edit_profile.view

import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface EditProfileMvpView: MvpView {
    fun loadDataDokter(data: GetDokterResponse)
    fun openHome()
}