package com.example.melanomaappfordoctor.ui.main.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.main.interactor.MainMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.MainMvpView

interface MainMvpPresenter<V: MainMvpView, I: MainMvpInteractor>: MvpPresenter<V, I> {
}