package com.example.melanomaappfordoctor.ui.main.view.fragments.history.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor.HistoryPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.view.HistoryPeriksaMvpView

interface HistoryPeriksaMvpPresenter<V: HistoryPeriksaMvpView, I: HistoryPeriksaMvpInteractor>: MvpPresenter<V, I> {
    fun getDataHistory()
}