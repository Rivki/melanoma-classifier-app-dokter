package com.example.melanomaappfordoctor.ui.edit_periksa.presenter

import com.example.melanomaappfordoctor.data.network.request.EditPeriksaRequest
import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.edit_periksa.interactor.EditPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_periksa.view.EditPeriksaMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import javax.inject.Inject

class EditPeriksaPresenter<V : EditPeriksaMvpView, I : EditPeriksaMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), EditPeriksaMvpPresenter<V, I> {
    override fun sendDataPeriksa(idPeriksa: String, kesimpulan: String, deskripsi: String) {
        val request = EditPeriksaRequest()
        request.idPeriksa = idPeriksa
        request.kesimpulan = kesimpulan
        request.deskripsi = deskripsi

        interactor?.let {
            compositeDisposable.add(
                it.editPeriksa(request)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response ->
                        if (response.isSuccessfull()){
                            getView()?.afterSendData()
                        }
                    }, { e ->
                        when(e){
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }
}