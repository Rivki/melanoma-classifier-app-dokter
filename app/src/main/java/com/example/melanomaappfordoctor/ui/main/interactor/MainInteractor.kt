package com.example.melanomaappfordoctor.ui.main.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class MainInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper), MainMvpInteractor {
}