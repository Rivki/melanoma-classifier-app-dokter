package com.example.melanomaappfordoctor.ui.edit_periksa

import com.example.melanomaappfordoctor.ui.edit_periksa.interactor.EditPeriksaInteractor
import com.example.melanomaappfordoctor.ui.edit_periksa.interactor.EditPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_periksa.presenter.EditPeriksaMvpPresenter
import com.example.melanomaappfordoctor.ui.edit_periksa.presenter.EditPeriksaPresenter
import com.example.melanomaappfordoctor.ui.edit_periksa.view.EditPeriksaMvpView
import dagger.Module
import dagger.Provides

@Module
class EditPeriksaActivityModule {
    @Provides
    fun provideEditPeriksaInteractor(interactor: EditPeriksaInteractor): EditPeriksaMvpInteractor =
        interactor

    @Provides
    fun provideEditPeriksaPresenter(presenter: EditPeriksaPresenter<EditPeriksaMvpView, EditPeriksaMvpInteractor>): EditPeriksaMvpPresenter<EditPeriksaMvpView, EditPeriksaMvpInteractor> =
        presenter
}