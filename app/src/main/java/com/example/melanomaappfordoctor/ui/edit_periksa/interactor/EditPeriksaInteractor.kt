package com.example.melanomaappfordoctor.ui.edit_periksa.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.request.EditPeriksaRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.EditPeriksaResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class EditPeriksaInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper), EditPeriksaMvpInteractor {
    override fun editPeriksa(request: EditPeriksaRequest): Observable<BaseResponse<EditPeriksaResponse>> =
        apiHelper.editPeriksa(request)
}