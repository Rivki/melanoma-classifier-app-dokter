package com.example.melanomaappfordoctor.ui.main.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.main.interactor.MainMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.MainMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenter<V : MainMvpView, I : MainMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = disposable
), MainMvpPresenter<V, I> {

}