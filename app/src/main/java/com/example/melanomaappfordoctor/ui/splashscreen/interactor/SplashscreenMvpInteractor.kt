package com.example.melanomaappfordoctor.ui.splashscreen.interactor

import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor

interface SplashscreenMvpInteractor: MvpInteractor {
    fun getToken(): Boolean
}