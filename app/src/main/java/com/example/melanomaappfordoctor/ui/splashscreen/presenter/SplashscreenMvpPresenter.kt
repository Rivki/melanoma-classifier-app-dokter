package com.example.melanomaappfordoctor.ui.splashscreen.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.splashscreen.interactor.SplashscreenMvpInteractor
import com.example.melanomaappfordoctor.ui.splashscreen.view.SplashscreenMvpView

interface SplashscreenMvpPresenter<V : SplashscreenMvpView, I : SplashscreenMvpInteractor> : MvpPresenter<V, I> {
    fun checkToken()
}