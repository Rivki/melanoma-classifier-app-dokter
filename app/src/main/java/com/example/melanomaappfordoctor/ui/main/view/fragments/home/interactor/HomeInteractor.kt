package com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.data.network.response.ListPeriksaResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class HomeInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper), HomeMvpInteractor {
    override fun getListPeriksa(): Observable<BaseResponse<ListPeriksaResponse>> = apiHelper.listPeriksa()
    override fun getDokter(): Observable<BaseResponse<GetDokterResponse>> = apiHelper.getDokter()
    override fun deleteToken() {
        preferencesHelper.resetAllPreferences()
    }
}