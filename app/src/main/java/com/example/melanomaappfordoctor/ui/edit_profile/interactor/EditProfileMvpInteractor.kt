package com.example.melanomaappfordoctor.ui.edit_profile.interactor

import com.example.melanomaappfordoctor.data.network.request.EditProfileRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.EditProfileResponse
import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface EditProfileMvpInteractor: MvpInteractor{
    fun editProfile(request: EditProfileRequest): Observable<BaseResponse<EditProfileResponse>>

    fun getUser(): Observable<BaseResponse<GetDokterResponse>>
}