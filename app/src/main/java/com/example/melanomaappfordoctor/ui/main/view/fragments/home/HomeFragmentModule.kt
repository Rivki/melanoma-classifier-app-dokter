package com.example.melanomaappfordoctor.ui.main.view.fragments.home

import com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor.HomeInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor.HomeMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.presenter.HomeMvpPresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.presenter.HomePresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.view.HomeMvpView
import dagger.Module
import dagger.Provides

@Module
class HomeFragmentModule {
    @Provides
    fun provideHomeInteractor(interactor: HomeInteractor): HomeMvpInteractor = interactor

    @Provides
    fun provideHomePresenter(presenter: HomePresenter<HomeMvpView, HomeMvpInteractor>): HomeMvpPresenter<HomeMvpView, HomeMvpInteractor> =
        presenter
}