package com.example.melanomaappfordoctor.ui.base.view

import androidx.annotation.StringRes

interface MvpView {
    fun showMessage(message: String?)
    fun showMessage(@StringRes resId: Int)
    fun showLoading()
    fun hideLoading()
    fun onTokenExpired()
}