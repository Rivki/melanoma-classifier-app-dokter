package com.example.melanomaappfordoctor.ui.splashscreen.view

import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface SplashscreenMvpView: MvpView {
    fun redirectToHome()
    fun redirectToLogin()
}