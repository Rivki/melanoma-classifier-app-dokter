package com.example.melanomaappfordoctor.ui.edit_periksa.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.edit_periksa.interactor.EditPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_periksa.view.EditPeriksaMvpView

interface EditPeriksaMvpPresenter<V: EditPeriksaMvpView, I: EditPeriksaMvpInteractor>: MvpPresenter<V, I> {
    fun sendDataPeriksa(idPeriksa: String, kesimpulan: String, deskripsi: String)
}