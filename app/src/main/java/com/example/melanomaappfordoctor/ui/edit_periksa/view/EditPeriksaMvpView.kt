package com.example.melanomaappfordoctor.ui.edit_periksa.view

import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface EditPeriksaMvpView: MvpView {
    fun afterSendData()
}