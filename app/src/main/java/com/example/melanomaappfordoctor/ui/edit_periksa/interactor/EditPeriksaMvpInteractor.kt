package com.example.melanomaappfordoctor.ui.edit_periksa.interactor

import com.example.melanomaappfordoctor.data.network.request.EditPeriksaRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.EditPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface EditPeriksaMvpInteractor: MvpInteractor {
    fun editPeriksa(request : EditPeriksaRequest): Observable<BaseResponse<EditPeriksaResponse>>
}