package com.example.melanomaappfordoctor.ui.main.view.fragments

import com.example.melanomaappfordoctor.ui.main.view.fragments.history.HistoryPeriksaFragmentModule
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.view.HistoryPeriksaFragment
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.HomeFragmentModule
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.view.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentProvider {
    @ContributesAndroidInjector(modules = [(HomeFragmentModule::class)])
    internal abstract fun provideHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [(HistoryPeriksaFragmentModule::class)])
    internal abstract fun provideHistoryPeriksaFragment(): HistoryPeriksaFragment
}