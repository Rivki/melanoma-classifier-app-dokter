package com.example.melanomaappfordoctor.ui.register.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.request.RegisterRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.RegisterResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class RegisterInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper), RegisterMvpInteractor {
    override fun register(request: RegisterRequest): Observable<BaseResponse<RegisterResponse>> = apiHelper.register(request)

    override fun setToken(token: String) {
        preferencesHelper.setToken(token)
    }

}