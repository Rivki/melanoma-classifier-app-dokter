package com.example.melanomaappfordoctor.ui.register

import com.example.melanomaappfordoctor.ui.register.interactor.RegisterInteractor
import com.example.melanomaappfordoctor.ui.register.interactor.RegisterMvpInteractor
import com.example.melanomaappfordoctor.ui.register.presenter.RegisterMvpPresenter
import com.example.melanomaappfordoctor.ui.register.presenter.RegisterPresenter
import com.example.melanomaappfordoctor.ui.register.view.RegisterMvpView
import dagger.Module
import dagger.Provides

@Module
class RegisterActivityModule {
    @Provides
    fun provideRegisterInteractor(interactor: RegisterInteractor): RegisterMvpInteractor =
        interactor

    @Provides
    fun provideRegisterPresenter(presenter: RegisterPresenter<RegisterMvpView, RegisterMvpInteractor>): RegisterMvpPresenter<RegisterMvpView, RegisterMvpInteractor> =
        presenter
}