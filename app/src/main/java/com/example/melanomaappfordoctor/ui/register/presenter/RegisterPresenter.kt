package com.example.melanomaappfordoctor.ui.register.presenter

import com.example.melanomaappfordoctor.data.network.request.RegisterRequest
import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.register.interactor.RegisterMvpInteractor
import com.example.melanomaappfordoctor.ui.register.view.RegisterMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import javax.inject.Inject

class RegisterPresenter<V : RegisterMvpView, I : RegisterMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), RegisterMvpPresenter<V, I> {
    override fun postDataRegister(nama: String, email: String, password: String, deviceId: String) {
        val request = RegisterRequest()
        request.nama = nama
        request.email = email
        request.password = password
        request.deviceId = deviceId

        interactor?.let {
            compositeDisposable.add(
                it.register(request)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response ->
                        if (response.isSuccessfull()) {
                            it.setToken(response.result.token)
                            getView()?.redirectToHome()
                        }
                    }, { e ->
                        when (e) {
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }

}