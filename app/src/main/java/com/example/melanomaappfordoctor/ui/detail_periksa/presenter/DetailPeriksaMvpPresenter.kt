package com.example.melanomaappfordoctor.ui.detail_periksa.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.detail_periksa.interactor.DetailPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.detail_periksa.view.DetailPeriksaMvpView

interface DetailPeriksaMvpPresenter<V: DetailPeriksaMvpView, I: DetailPeriksaMvpInteractor>: MvpPresenter<V, I> {
    fun loadDetailPeriksa(id: Int)
}