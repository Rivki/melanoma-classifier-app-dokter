package com.example.melanomaappfordoctor.ui.login.view

import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface LoginMvpView : MvpView {
    fun redirectToHome()
}