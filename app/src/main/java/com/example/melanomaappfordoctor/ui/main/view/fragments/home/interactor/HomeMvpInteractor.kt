package com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor

import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.data.network.response.ListPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface HomeMvpInteractor: MvpInteractor {
    fun getListPeriksa(): Observable<BaseResponse<ListPeriksaResponse>>
    fun getDokter(): Observable<BaseResponse<GetDokterResponse>>
    fun deleteToken()
}