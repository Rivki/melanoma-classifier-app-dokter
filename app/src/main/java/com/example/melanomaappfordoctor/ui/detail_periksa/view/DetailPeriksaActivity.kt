package com.example.melanomaappfordoctor.ui.detail_periksa.view

import android.annotation.SuppressLint
import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import com.example.melanomaappfordoctor.BuildConfig
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.data.network.response.DetailPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.view.BaseActivity
import com.example.melanomaappfordoctor.ui.detail_periksa.interactor.DetailPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.detail_periksa.presenter.DetailPeriksaMvpPresenter
import com.example.melanomaappfordoctor.ui.edit_periksa.view.EditPeriksaActivity
import com.example.melanomaappfordoctor.util.CommonUtil.convertToPercent
import com.example.melanomaappfordoctor.util.CommonUtil.formatDates
import com.example.melanomaappfordoctor.util.loadImage
import kotlinx.android.synthetic.main.activity_detail_periksa.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class DetailPeriksaActivity : BaseActivity(), DetailPeriksaMvpView {

    @Inject
    lateinit var presenter: DetailPeriksaMvpPresenter<DetailPeriksaMvpView, DetailPeriksaMvpInteractor>
    private var status = false

    companion object {
        const val ID_PERIKSA = "ID_PERIKSA"
        const val STATUS = "STATUS"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_periksa)
        setupToolbar("Pemeriksaan")
        setup()
    }

    private fun setup() {
        val idPeriksa = intent.getIntExtra(ID_PERIKSA, 0)
        status = intent.getBooleanExtra(STATUS, false)
        presenter.let {
            it.onAttach(this)
            it.loadDetailPeriksa(idPeriksa)
        }
    }

    override fun onFragmentAttached() {}

    override fun onFragmentDetached(tag: String) {}

    @SuppressLint("SetTextI18n")
    override fun loadDataDetail(data: DetailPeriksaResponse) {
        imageDetailPeriksa.loadImage(BuildConfig.BASE_URL_IMAGE + data.gambar)
        textViewNamaDokter.text = data.nama
        textViewStatusPeriksa.text = "${data.hasilPrediksi.convertToPercent()}%"
        textViewTanggalPeriksa.text = data.createAt.formatDates()
        textViewKeluhan.text = data.keluhan
        textViewUmur.text = data.umur.toString()
        textViewRiwayatPenyakit.text = data.riwayatPenyakit
        statusDetail(data)
    }

    private fun statusDetail(data: DetailPeriksaResponse) {
        when (status) {
            true -> {
                buttonKesimpulan.visibility = View.GONE
                layoutKesimpulan.visibility = View.VISIBLE
                view.visibility = View.VISIBLE
                textViewKesimpulan.text = data.kesimpulan.toString()
                textViewDeskripsi.text = data.deskripsi.toString()
            }
            false -> {
                layoutKesimpulan.visibility = View.GONE
                view.visibility = View.GONE
                buttonKesimpulan.apply {
                    visibility = View.VISIBLE
                    setOnClickListener {
                        startActivity<EditPeriksaActivity>(
                            EditPeriksaActivity.IMAGE_NAME to data.gambar,
                            EditPeriksaActivity.ID_PERIKSA to data.id
                        )
                    }
                }
            }
        }
    }
}