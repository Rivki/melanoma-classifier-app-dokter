package com.example.melanomaappfordoctor.ui.detail_periksa.view

import com.example.melanomaappfordoctor.data.network.response.DetailPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface DetailPeriksaMvpView: MvpView {
    fun loadDataDetail(data: DetailPeriksaResponse)
}