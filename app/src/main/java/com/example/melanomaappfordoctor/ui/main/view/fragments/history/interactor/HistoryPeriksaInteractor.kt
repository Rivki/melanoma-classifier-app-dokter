package com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.HistoryPeriksaResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class HistoryPeriksaInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper),
    HistoryPeriksaMvpInteractor {
    override fun historyPeriksa(): Observable<BaseResponse<HistoryPeriksaResponse>> = apiHelper.historyPeriksa()
}