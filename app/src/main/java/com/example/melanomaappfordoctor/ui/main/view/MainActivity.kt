package com.example.melanomaappfordoctor.ui.main.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.ui.base.view.BaseActivity
import com.example.melanomaappfordoctor.ui.main.interactor.MainMvpInteractor
import com.example.melanomaappfordoctor.ui.main.presenter.MainMvpPresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.view.HistoryPeriksaFragment
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.view.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainMvpView, HasSupportFragmentInjector {

    @Inject
    lateinit var presenter: MainMvpPresenter<MainMvpView, MainMvpInteractor>

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    companion object{
        const val SCREEN_ROUTE = "screen"

        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    private val mOnNavigateItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item->
        when (item.itemId) {
            R.id.home -> {
                val fragment = HomeFragment.newInstance()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.history -> {
                val fragment = HistoryPeriksaFragment.newInstance()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var screen : String? = null

        val extra = intent.extras
        if(extra != null){
            screen = extra.getString(SCREEN_ROUTE)
        }

        Log.d("SCREEN", screen.toString())

        routeScreen(screen)

        presenter.onAttach(this)
        main_bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigateItemSelectedListener)
        val fragment = HomeFragment.newInstance()
        addFragment(fragment)
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_frame, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    private fun routeScreen(screen: String?){
        when(screen){
            getString(R.string.screen_home) -> {
                val fragment = HomeFragment.newInstance()
                addFragment(fragment)
//                main_bottom_navigation.selectedItemId = R.id.home
            }
            getString(R.string.screen_history) -> {
                val fragment = HistoryPeriksaFragment.newInstance()
                addFragment(fragment)
//                main_bottom_navigation.selectedItemId = R.id.history
            }
        }
    }

    override fun onFragmentAttached() {}

    override fun onFragmentDetached(tag: String) {}

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector
}
