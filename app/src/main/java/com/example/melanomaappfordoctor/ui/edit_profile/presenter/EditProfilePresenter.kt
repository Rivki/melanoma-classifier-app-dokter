package com.example.melanomaappfordoctor.ui.edit_profile.presenter

import com.example.melanomaappfordoctor.data.network.request.EditProfileRequest
import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.edit_profile.interactor.EditProfileMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_profile.view.EditProfileMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import java.io.File
import javax.inject.Inject

class EditProfilePresenter<V : EditProfileMvpView, I : EditProfileMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), EditProfileMvpPresenter<V, I> {
    override fun postData(
        file: File?,
        nama: String,
        jk: String,
        noTelepon: String,
        alamat: String,
        tempat_tanggal_lahir: String,
        tanggal_lahir: String,
        umur: Int
    ) {
        val request = EditProfileRequest()
        request.nama = nama
        request.jenisKelamin = jk
        request.noTelepon = noTelepon
        request.alamat = alamat
        request.file = file
        request.tempatTanggalLahir = tempat_tanggal_lahir
        request.tanggalLahir = tanggal_lahir
        request.umur = umur

        interactor?.let {
            compositeDisposable.add(
                it.editProfile(request)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response ->
                        if (response.isSuccessfull()) {
                            getView()?.openHome()
                        }
                    }, { e ->
                        when (e) {
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }

    override fun loadData() {
        interactor?.let {
            compositeDisposable.add(
                it.getUser()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response ->
                        getView()?.loadDataDokter(response.result)
                    }, { e ->
                        when (e) {
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }
}