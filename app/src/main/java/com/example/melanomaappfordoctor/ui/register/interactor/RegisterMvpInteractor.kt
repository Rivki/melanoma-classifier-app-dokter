package com.example.melanomaappfordoctor.ui.register.interactor

import com.example.melanomaappfordoctor.data.network.request.RegisterRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.RegisterResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface RegisterMvpInteractor: MvpInteractor {
    fun register(request: RegisterRequest): Observable<BaseResponse<RegisterResponse>>

    fun setToken(token: String)
}