package com.example.melanomaappfordoctor.ui.edit_profile.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.edit_profile.interactor.EditProfileMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_profile.view.EditProfileMvpView
import java.io.File

interface EditProfileMvpPresenter<V : EditProfileMvpView, I : EditProfileMvpInteractor> :
    MvpPresenter<V, I> {
    fun postData(
        file: File?,
        nama: String,
        jk: String,
        noTelepon: String,
        alamat: String,
        tempat_tanggal_lahir: String,
        tanggal_lahir: String,
        umur: Int
    )

    fun loadData()
}