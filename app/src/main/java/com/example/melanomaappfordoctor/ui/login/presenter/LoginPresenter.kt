package com.example.melanomaappfordoctor.ui.login.presenter

import com.example.melanomaappfordoctor.data.network.request.LoginRequest
import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.login.interactor.LoginMvpInteractor
import com.example.melanomaappfordoctor.ui.login.view.LoginMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import javax.inject.Inject

class LoginPresenter<V : LoginMvpView, I : LoginMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
): BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), LoginMvpPresenter<V, I> {
    override fun login(email: String, password: String, deviceId: String) {
        val request = LoginRequest()
        request.email = email
        request.password = password
        request.deviceId = deviceId

        interactor?.let {
            compositeDisposable.add(
                it.login(request)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response->
                        if(response.isSuccessfull()){
                            getView()?.redirectToHome()
                            it.saveToken(response.result.token)
                            it.setId(response.result.id)
                        }else{
                            getView()?.showMessage(response.error)
                        }
                    }, {e->
                        when(e){
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }

}