package com.example.melanomaappfordoctor.ui.splashscreen

import com.example.melanomaappfordoctor.ui.splashscreen.interactor.SplashscreenInteractor
import com.example.melanomaappfordoctor.ui.splashscreen.interactor.SplashscreenMvpInteractor
import com.example.melanomaappfordoctor.ui.splashscreen.presenter.SplashscreenMvpPresenter
import com.example.melanomaappfordoctor.ui.splashscreen.presenter.SplashscreenPresenter
import com.example.melanomaappfordoctor.ui.splashscreen.view.SplashscreenMvpView
import dagger.Module
import dagger.Provides

@Module
class SplashscreenActivityModule {

    @Provides
    fun provideSplashscreenInteractor(interactor: SplashscreenInteractor): SplashscreenMvpInteractor =
        interactor

    @Provides
    fun provideSplashscreenPresenter(presenter: SplashscreenPresenter<SplashscreenMvpView, SplashscreenMvpInteractor>): SplashscreenMvpPresenter<SplashscreenMvpView, SplashscreenMvpInteractor> =
        presenter
}