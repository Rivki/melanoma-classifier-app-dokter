package com.example.melanomaappfordoctor.ui.main.view.fragments.home.view

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.data.network.response.PeriksaResponse
import kotlinx.android.synthetic.main.item_view.view.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class HomeAdapter(
    val list: List<PeriksaResponse> = listOf(),
    val listener: (PeriksaResponse) -> Unit
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindModel(list[position], listener)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tanggalPeriksa = itemView.tv_tgl_periksa
        val keluhan = itemView.tv_keluhan

        @SuppressLint("SetTextI18n")
        fun bindModel(periksa: PeriksaResponse, listener: (PeriksaResponse) -> Unit) {

            var formattedDate = ""
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val inputFormat =
                    DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS", Locale.getDefault())
                val outputFormat = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.getDefault())
                val date = LocalDate.parse(periksa.createAt, inputFormat)
                formattedDate = outputFormat.format(date)
            } else {
                val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
                val outputFormat = SimpleDateFormat("dd MMMM yyyy")
                val date = inputFormat.parse(periksa.createAt)
                formattedDate = outputFormat.format(date)
            }

            keluhan.text = "Keluhan: ${periksa.keluhan}"
            tanggalPeriksa.text = "Tanggal Periksa: $formattedDate"
            itemView.setOnClickListener {
                listener(periksa)
            }
        }
    }
}