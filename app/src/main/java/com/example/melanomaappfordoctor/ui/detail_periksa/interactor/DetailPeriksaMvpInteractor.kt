package com.example.melanomaappfordoctor.ui.detail_periksa.interactor

import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.DetailPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface DetailPeriksaMvpInteractor: MvpInteractor {
    fun getDetailPeriksa(id: Int): Observable<BaseResponse<DetailPeriksaResponse>>
}