package com.example.melanomaappfordoctor.ui.main.view.fragments.history.view

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.data.network.response.PeriksaResponse
import com.example.melanomaappfordoctor.util.CommonUtil
import com.example.melanomaappfordoctor.util.CommonUtil.formatDates
import kotlinx.android.synthetic.main.item_view.view.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class HistoryPeriksaAdapter(
    val list: List<PeriksaResponse>,
    val listener: (PeriksaResponse) -> Unit
) : RecyclerView.Adapter<HistoryPeriksaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(list[position], listener)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tanggalPeriksa = itemView.tv_tgl_periksa
        private val keluhan = itemView.tv_keluhan

        @SuppressLint("SetTextI18n")
        fun bindItem(data: PeriksaResponse, listener: (PeriksaResponse) -> Unit) {
            keluhan.text = "Keluhan: ${data.keluhan}"
            tanggalPeriksa.text = "Tanggal Periksa: ${data.createAt.formatDates()}"
            itemView.setOnClickListener {
                listener(data)
            }
        }

    }
}