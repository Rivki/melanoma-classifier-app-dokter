package com.example.melanomaappfordoctor.ui.register.view

import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface RegisterMvpView: MvpView {
    fun redirectToHome()
}