package com.example.melanomaappfordoctor.ui.main.view.fragments.home.view

import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.data.network.response.ListPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface HomeMvpView: MvpView {
    fun loadDataPeriksa(data: ListPeriksaResponse)
    fun loadDataDokter(data: GetDokterResponse)
    fun logoutAction()
}