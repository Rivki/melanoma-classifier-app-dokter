package com.example.melanomaappfordoctor.ui.main.view.fragments.history

import com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor.HistoryPeriksaInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor.HistoryPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.presenter.HistoryPeriksaMvpPresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.presenter.HistoryPeriksaPresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.view.HistoryPeriksaMvpView
import dagger.Module
import dagger.Provides

@Module
class HistoryPeriksaFragmentModule {
    @Provides
    fun provideHistoryPeriksaInteractor(interactor: HistoryPeriksaInteractor): HistoryPeriksaMvpInteractor =
        interactor

    @Provides
    fun provideHistoryPeriksaPresenter(presenter: HistoryPeriksaPresenter<HistoryPeriksaMvpView, HistoryPeriksaMvpInteractor>): HistoryPeriksaMvpPresenter<HistoryPeriksaMvpView, HistoryPeriksaMvpInteractor> =
        presenter
}