package com.example.melanomaappfordoctor.ui.login

import com.example.melanomaappfordoctor.ui.login.interactor.LoginInteractor
import com.example.melanomaappfordoctor.ui.login.interactor.LoginMvpInteractor
import com.example.melanomaappfordoctor.ui.login.presenter.LoginMvpPresenter
import com.example.melanomaappfordoctor.ui.login.presenter.LoginPresenter
import com.example.melanomaappfordoctor.ui.login.view.LoginMvpView
import dagger.Module
import dagger.Provides

@Module
class LoginModuleActivity {
    @Provides
    fun provideLoginInteractor(interactor: LoginInteractor): LoginMvpInteractor = interactor

    @Provides
    fun provideLoginPresenter(presenter: LoginPresenter<LoginMvpView, LoginMvpInteractor>): LoginMvpPresenter<LoginMvpView, LoginMvpInteractor> =
        presenter
}