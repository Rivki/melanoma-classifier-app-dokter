package com.example.melanomaappfordoctor.ui.edit_profile.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.request.EditProfileRequest
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.EditProfileResponse
import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class EditProfileInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper), EditProfileMvpInteractor {
    override fun editProfile(request: EditProfileRequest): Observable<BaseResponse<EditProfileResponse>> =
        apiHelper.editProfile(request)

    override fun getUser(): Observable<BaseResponse<GetDokterResponse>> = apiHelper.getDokter()
}