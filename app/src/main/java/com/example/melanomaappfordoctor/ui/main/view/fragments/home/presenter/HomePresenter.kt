package com.example.melanomaappfordoctor.ui.main.view.fragments.home.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor.HomeMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.view.HomeMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import javax.inject.Inject

class HomePresenter<V: HomeMvpView, I: HomeMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
): BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), HomeMvpPresenter<V, I>{
    override fun loadListPeriksa() {
        interactor?.let {
            compositeDisposable.add(
                it.getListPeriksa()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response->
                        if(response.isSuccessfull()){
                            getView()?.loadDataPeriksa(response.result)
                        }
                    }, { e->
                        when(e){
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }

    override fun loadDataDokter() {
        interactor?.let {
            compositeDisposable.add(
                it.getDokter()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({ response->
                        if(response.isSuccessfull()){
                            getView()?.loadDataDokter(response.result)
                        }
                    }, { e->
                        when(e){
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }

    override fun logout() {
        interactor?.let {
            it.deleteToken()
            getView()?.logoutAction()
        }
    }
}