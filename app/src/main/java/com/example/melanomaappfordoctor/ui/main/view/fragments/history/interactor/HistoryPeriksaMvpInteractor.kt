package com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor

import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.HistoryPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import io.reactivex.Observable

interface HistoryPeriksaMvpInteractor: MvpInteractor {
    fun historyPeriksa(): Observable<BaseResponse<HistoryPeriksaResponse>>
}