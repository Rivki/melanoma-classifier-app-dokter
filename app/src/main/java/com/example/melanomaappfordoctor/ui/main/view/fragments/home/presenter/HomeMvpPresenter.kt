package com.example.melanomaappfordoctor.ui.main.view.fragments.home.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor.HomeMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.view.HomeMvpView

interface HomeMvpPresenter<V : HomeMvpView, I : HomeMvpInteractor> : MvpPresenter<V, I> {
    fun loadListPeriksa()
    fun loadDataDokter()
    fun logout()
}