package com.example.melanomaappfordoctor.ui.edit_profile

import com.example.melanomaappfordoctor.ui.edit_profile.interactor.EditProfileInteractor
import com.example.melanomaappfordoctor.ui.edit_profile.interactor.EditProfileMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_profile.presenter.EditProfileMvpPresenter
import com.example.melanomaappfordoctor.ui.edit_profile.presenter.EditProfilePresenter
import com.example.melanomaappfordoctor.ui.edit_profile.view.EditProfileMvpView
import dagger.Module
import dagger.Provides

@Module
class EditProfileActivityModule {
    @Provides
    fun provideEditProfileInteractor(interactor: EditProfileInteractor): EditProfileMvpInteractor = interactor

    @Provides
    fun provideEditProfilePresenter(presenter: EditProfilePresenter<EditProfileMvpView, EditProfileMvpInteractor>): EditProfileMvpPresenter<EditProfileMvpView, EditProfileMvpInteractor> = presenter
}