package com.example.melanomaappfordoctor.ui.splashscreen.view

import android.os.Bundle
import android.os.Handler
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.ui.base.view.BaseActivity
import com.example.melanomaappfordoctor.ui.login.view.LoginActivity
import com.example.melanomaappfordoctor.ui.main.view.MainActivity
import com.example.melanomaappfordoctor.ui.splashscreen.interactor.SplashscreenMvpInteractor
import com.example.melanomaappfordoctor.ui.splashscreen.presenter.SplashscreenMvpPresenter
import com.github.ybq.android.spinkit.style.FoldingCube
import kotlinx.android.synthetic.main.splashscreen_activity.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class SplashscreenActivity : BaseActivity(), SplashscreenMvpView {

    @Inject
    lateinit var presenter: SplashscreenMvpPresenter<SplashscreenMvpView, SplashscreenMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashscreen_activity)
        presenter.onAttach(this)

        val foldingCube = FoldingCube()
        progress.setIndeterminateDrawable(foldingCube)

        Handler().postDelayed({
            presenter.checkToken()
        }, 1000)
    }

    override fun onFragmentAttached() {}

    override fun onFragmentDetached(tag: String) {}

    override fun redirectToHome() {
        startActivity<MainActivity>()
        finish()
    }

    override fun redirectToLogin() {
        startActivity<LoginActivity>()
        finish()
    }
}