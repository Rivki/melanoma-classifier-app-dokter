package com.example.melanomaappfordoctor.ui.splashscreen.presenter

import android.util.Log
import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.splashscreen.interactor.SplashscreenMvpInteractor
import com.example.melanomaappfordoctor.ui.splashscreen.view.SplashscreenMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashscreenPresenter<V : SplashscreenMvpView, I : SplashscreenMvpInteractor>
@Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), SplashscreenMvpPresenter<V, I> {
    override fun checkToken() {
        Log.d("CHECK_TOKEN", interactor?.getToken().toString())
        when (interactor?.getToken()) {
            true -> getView()?.redirectToHome()
            false -> getView()?.redirectToLogin()
        }
    }

}