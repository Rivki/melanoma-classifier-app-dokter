package com.example.melanomaappfordoctor.ui.edit_periksa.view

import android.os.Bundle
import com.example.melanomaappfordoctor.BuildConfig
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.ui.base.view.BaseActivity
import com.example.melanomaappfordoctor.ui.edit_periksa.interactor.EditPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.edit_periksa.presenter.EditPeriksaMvpPresenter
import com.example.melanomaappfordoctor.ui.main.view.MainActivity
import com.example.melanomaappfordoctor.util.loadImage
import kotlinx.android.synthetic.main.activity_edit_periksa.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class EditPeriksaActivity: BaseActivity(), EditPeriksaMvpView {

    companion object{
        const val IMAGE_NAME = "IMAGE_NAME"
        const val ID_PERIKSA = "ID_PERIKSA"
    }

    @Inject
    lateinit var presenter: EditPeriksaMvpPresenter<EditPeriksaMvpView, EditPeriksaMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_periksa)
        setup()
    }

    private fun setup(){
        val imageName = intent.getStringExtra(IMAGE_NAME)
        val idPeriksa = intent.getIntExtra(ID_PERIKSA, 0)

        presenter.onAttach(this)

        imageEditPeriksa.loadImage(BuildConfig.BASE_URL_IMAGE + imageName)

        buttonKesimpulan.setOnClickListener {
            val kesimpulan = if(kesimpulanRadioButtonGroup.checkedRadioButtonId != 0){
                if (kesimpulanRadioButtonGroup.checkedRadioButtonId == R.id.positifRadioButton) "positif" else "negatif"
            }else{
                ""
            }
            val deskripsi = editTextDeskripsi.text.toString()
            presenter.sendDataPeriksa(idPeriksa.toString(), kesimpulan, deskripsi)
        }
    }

    override fun onFragmentAttached() {}

    override fun onFragmentDetached(tag: String) {}

    override fun afterSendData() {
        startActivity<MainActivity>()
        finish()
    }
}