package com.example.melanomaappfordoctor.ui.register.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.register.interactor.RegisterMvpInteractor
import com.example.melanomaappfordoctor.ui.register.view.RegisterMvpView

interface RegisterMvpPresenter<V: RegisterMvpView, I: RegisterMvpInteractor>: MvpPresenter<V, I> {
    fun postDataRegister(nama: String, email: String, password: String, deviceId: String)
}