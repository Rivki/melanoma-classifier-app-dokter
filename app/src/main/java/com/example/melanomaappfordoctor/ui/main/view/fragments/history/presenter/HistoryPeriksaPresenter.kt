package com.example.melanomaappfordoctor.ui.main.view.fragments.history.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.BasePresenter
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.interactor.HistoryPeriksaMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.history.view.HistoryPeriksaMvpView
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import javax.inject.Inject

class HistoryPeriksaPresenter<V : HistoryPeriksaMvpView, I : HistoryPeriksaMvpInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
),
    HistoryPeriksaMvpPresenter<V, I> {
    override fun getDataHistory() {
        interactor?.let {
            compositeDisposable.add(
                it.historyPeriksa()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .doOnSubscribe { getView()?.showLoading() }
                    .doOnTerminate { getView()?.hideLoading() }
                    .subscribe({response->
                        if(response.isSuccessfull()){
                            getView()?.loadDataHistory(response.result)
                        }
                    }, {e->
                        when(e){
                            is HttpException -> handleApiError(e)
                            else -> handleGenericError(e.localizedMessage)
                        }
                    })
            )
        }
    }

}