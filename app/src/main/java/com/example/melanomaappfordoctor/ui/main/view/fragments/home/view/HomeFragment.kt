package com.example.melanomaappfordoctor.ui.main.view.fragments.home.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.melanomaappfordoctor.BuildConfig
import com.example.melanomaappfordoctor.R
import com.example.melanomaappfordoctor.data.network.response.GetDokterResponse
import com.example.melanomaappfordoctor.data.network.response.ListPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.view.BaseFragment
import com.example.melanomaappfordoctor.ui.detail_periksa.view.DetailPeriksaActivity
import com.example.melanomaappfordoctor.ui.edit_profile.view.EditProfileActivity
import com.example.melanomaappfordoctor.ui.login.view.LoginActivity
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.interactor.HomeMvpInteractor
import com.example.melanomaappfordoctor.ui.main.view.fragments.home.presenter.HomeMvpPresenter
import com.example.melanomaappfordoctor.util.loadImage
import kotlinx.android.synthetic.main.app_bar_transparent.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.startActivity
import javax.inject.Inject

class HomeFragment : BaseFragment(), HomeMvpView {

    @Inject
    lateinit var presenter: HomeMvpPresenter<HomeMvpView, HomeMvpInteractor>
    private lateinit var rvAdapter: HomeAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(): HomeFragment {
            return HomeFragment().apply {
                arguments = Bundle()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun setUp() {
        presenter.let {
            it.onAttach(this)
            it.loadListPeriksa()
            it.loadDataDokter()
        }
        img_logout.setOnClickListener {
            alert("Apakah anda yakin ingin keluar?") {
                positiveButton("Ya") { dialog ->
                    presenter.logout()
                    dialog.dismiss()
                }

                negativeButton("Tidak") { dialog ->
                    dialog.dismiss()
                }
            }.show()
        }

        img_edit.setOnClickListener {
            startActivity<EditProfileActivity>()
        }
    }

    override fun loadDataPeriksa(data: ListPeriksaResponse) {
        if(data.data.isEmpty()){
            data_empty.visibility = View.VISIBLE
            list_data.visibility = View.GONE
        }else{
            list_data.visibility = View.VISIBLE
            data_empty.visibility = View.GONE
            val layoutManagers = LinearLayoutManager(activity)
            rv_periksa_pasien.apply {
                layoutManager = layoutManagers
                rvAdapter = HomeAdapter(data.data) {
                    startActivity<DetailPeriksaActivity>(
                        DetailPeriksaActivity.ID_PERIKSA to it.id
                    )
                }
                adapter = rvAdapter
            }
        }
    }

    override fun loadDataDokter(data: GetDokterResponse) {
        img_profile.loadImage(BuildConfig.BASE_URL_IMAGE + data.gambar)
        tv_name.text = data.nama
        tv_email.text = data.email
        ratingBar.rating = data.rate
    }

    override fun logoutAction() {
        startActivity<LoginActivity>()
    }
}