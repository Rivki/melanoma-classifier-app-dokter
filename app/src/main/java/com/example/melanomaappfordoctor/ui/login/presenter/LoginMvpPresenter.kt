package com.example.melanomaappfordoctor.ui.login.presenter

import com.example.melanomaappfordoctor.ui.base.presenter.MvpPresenter
import com.example.melanomaappfordoctor.ui.login.interactor.LoginMvpInteractor
import com.example.melanomaappfordoctor.ui.login.view.LoginMvpView

interface LoginMvpPresenter<V: LoginMvpView, I: LoginMvpInteractor> : MvpPresenter<V, I> {
    fun login(email: String, password: String, deviceId: String)
}