package com.example.melanomaappfordoctor.ui.base.presenter

import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import com.example.melanomaappfordoctor.ui.base.view.MvpView
import retrofit2.HttpException

interface MvpPresenter<V: MvpView, I: MvpInteractor> {
    fun onAttach(view: V?)
    fun onDetach()
    fun getView(): V?
    fun handleApiError(error: HttpException)
    fun handleGenericError(errorMessage: String?)
    fun performUserLogout()
}