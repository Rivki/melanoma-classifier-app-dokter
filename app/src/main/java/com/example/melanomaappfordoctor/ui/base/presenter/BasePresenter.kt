package com.example.melanomaappfordoctor.ui.base.presenter

import android.util.Log
import com.example.melanomaappfordoctor.data.network.response.ErrorResponse
import com.example.melanomaappfordoctor.ui.base.interactor.MvpInteractor
import com.example.melanomaappfordoctor.ui.base.view.MvpView
import com.example.melanomaappfordoctor.util.AppConstants
import com.example.melanomaappfordoctor.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

abstract class BasePresenter<V : MvpView, I : MvpInteractor> internal constructor(
    protected var interactor: I?,
    protected val schedulerProvider: SchedulerProvider,
    protected val compositeDisposable: CompositeDisposable
) : MvpPresenter<V, I> {
    private var views: V? = null
    private val isAttachedView: Boolean get() = views != null

    override fun onAttach(view: V?) {
        this.views = view
    }

    override fun getView(): V? = views

    override fun onDetach() {
        compositeDisposable.dispose()
        views = null
        interactor = null
    }

    override fun handleApiError(error: HttpException) {
        if(error.code() == AppConstants.ApiStatusCode.FORBIDDEN
            || error.code() == AppConstants.ApiStatusCode.BAD_REQUEST
            || error.code() == AppConstants.ApiStatusCode.UNAUTHORIZED){
            if (interactor?.isUserLoggedIn()!!){
                performUserLogout()
                return
            }
        }else if(error.code() == AppConstants.ApiStatusCode.BAD_REQUEST){
            views?.showMessage("Terjadi kesalahan pada server")
        }

        val errorMessage = ErrorResponse.fromRaw(error.response()?.errorBody()?.string()!!).message

        if(errorMessage != null){
            views?.showMessage(errorMessage)
        }else{
            views?.showMessage("Something went wrong")
        }
        Log.e("ERROR", error.localizedMessage)
    }

    override fun handleGenericError(errorMessage: String?) {
        errorMessage?.let {
            views?.showMessage(it)
        }
    }

    override fun performUserLogout() {
        interactor?.let {
            it.performUserLogout()
            getView()?.onTokenExpired()
        }
    }
}