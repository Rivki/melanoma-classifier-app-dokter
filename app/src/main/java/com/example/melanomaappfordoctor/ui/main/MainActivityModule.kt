package com.example.melanomaappfordoctor.ui.main

import com.example.melanomaappfordoctor.ui.main.interactor.MainInteractor
import com.example.melanomaappfordoctor.ui.main.interactor.MainMvpInteractor
import com.example.melanomaappfordoctor.ui.main.presenter.MainMvpPresenter
import com.example.melanomaappfordoctor.ui.main.presenter.MainPresenter
import com.example.melanomaappfordoctor.ui.main.view.MainMvpView
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule{
    @Provides
    internal fun provideMainInteractor(interactor: MainInteractor): MainMvpInteractor = interactor

    @Provides
    internal fun provideMainPresenter(presenter: MainPresenter<MainMvpView, MainMvpInteractor>): MainMvpPresenter<MainMvpView, MainMvpInteractor> = presenter

}