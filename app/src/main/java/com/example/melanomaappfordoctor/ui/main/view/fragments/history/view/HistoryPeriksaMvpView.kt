package com.example.melanomaappfordoctor.ui.main.view.fragments.history.view

import com.example.melanomaappfordoctor.data.network.response.HistoryPeriksaResponse
import com.example.melanomaappfordoctor.ui.base.view.MvpView

interface HistoryPeriksaMvpView: MvpView {
    fun loadDataHistory(data: HistoryPeriksaResponse)
}