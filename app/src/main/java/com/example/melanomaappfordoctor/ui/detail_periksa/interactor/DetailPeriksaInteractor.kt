package com.example.melanomaappfordoctor.ui.detail_periksa.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.network.response.BaseResponse
import com.example.melanomaappfordoctor.data.network.response.DetailPeriksaResponse
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class DetailPeriksaInteractor @Inject internal constructor(
    apiHelper: ApiHelper,
    preferencesHelper: PreferencesHelper
) : BaseInteractor(preferencesHelper, apiHelper), DetailPeriksaMvpInteractor {
    override fun getDetailPeriksa(id: Int): Observable<BaseResponse<DetailPeriksaResponse>> = apiHelper.getDetailPeriksa(id)
}