package com.example.melanomaappfordoctor.ui.splashscreen.interactor

import com.example.melanomaappfordoctor.data.network.ApiHelper
import com.example.melanomaappfordoctor.data.preferences.PreferencesHelper
import com.example.melanomaappfordoctor.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class SplashscreenInteractor @Inject internal constructor(
    preferencesHelper: PreferencesHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferencesHelper, apiHelper), SplashscreenMvpInteractor {
    override fun getToken(): Boolean = !preferencesHelper.getToken().isNullOrEmpty()

}