package com.example.melanomaappfordoctor.data.network

import com.example.melanomaappfordoctor.data.network.request.EditPeriksaRequest
import com.example.melanomaappfordoctor.data.network.request.EditProfileRequest
import com.example.melanomaappfordoctor.data.network.request.LoginRequest
import com.example.melanomaappfordoctor.data.network.request.RegisterRequest
import com.example.melanomaappfordoctor.data.network.response.*
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class AppApiHelper @Inject constructor(private val api: MainApi) : ApiHelper {
    override fun login(loginRequest: LoginRequest): Observable<BaseResponse<LoginResponse>> =
        api.login(loginRequest)

    override fun listPeriksa(): Observable<BaseResponse<ListPeriksaResponse>> = api.getListPeriksa()

    override fun getDokter(): Observable<BaseResponse<GetDokterResponse>> = api.getDokter()

    override fun getDetailPeriksa(id: Int): Observable<BaseResponse<DetailPeriksaResponse>> =
        api.getDetailPeriksa(id)

    override fun editPeriksa(request: EditPeriksaRequest): Observable<BaseResponse<EditPeriksaResponse>> {
        val idPeriksa = createPartFromString(request.idPeriksa ?: "")
        val kesimpulan = createPartFromString(request.kesimpulan ?: "")
        val deskripsi = createPartFromString(request.deskripsi ?: "")

        return api.editPeriksa(idPeriksa, kesimpulan, deskripsi)
    }

    override fun historyPeriksa(): Observable<BaseResponse<HistoryPeriksaResponse>> =
        api.historyPeriksa()

    override fun register(request: RegisterRequest): Observable<BaseResponse<RegisterResponse>> = api.register(request)

    override fun editProfile(request: EditProfileRequest): Observable<BaseResponse<EditProfileResponse>> {
        var imageFileBody: MultipartBody.Part? = null
        if (request.file != null) {
            val requestBody = RequestBody.create(MediaType.get("multipart/form-data"), request.file)
            imageFileBody =
                MultipartBody.Part.createFormData("file_upload", request.file!!.name, requestBody)
        }else{
            val requestBody = RequestBody.create(MediaType.get("multipart/form-data"), "")
            imageFileBody =
                MultipartBody.Part.createFormData("file_upload", "", requestBody)
        }

        val nama = createPartFromString(request.nama ?: "")
        val jk = createPartFromString(request.jenisKelamin ?: "")
        val alamat = createPartFromString(request.alamat ?: "")
        val noHp = createPartFromString(request.noTelepon ?: "")
        val umur = createPartFromString(request.umur.toString())
        val tempatTanggalLahir = createPartFromString(request.tempatTanggalLahir ?: "")
        val tanggalLahir = createPartFromString(request.tanggalLahir ?: "")

        return api.editProfile(imageFileBody, nama, noHp, jk, tempatTanggalLahir, tanggalLahir, umur, alamat)
    }

    private fun createPartFromString(param: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), param)
    }
}