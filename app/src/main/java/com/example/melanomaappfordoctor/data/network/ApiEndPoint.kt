package com.example.melanomaappfordoctor.data.network

object ApiEndPoint {
    const val LOGIN = "dokter/login"
    const val LIST_PERIKSA = "periksa/list_periksa_dokter"
    const val GET_DOKTER = "dokter/user"
    const val GET_DETAIL_PERIKSA = "periksa/detail_dokter/"
    const val EDIT_PERIKSA = "periksa/edit_periksa"
    const val HISTORY_PERIKSA = "periksa/history_periksa_dokter"
    const val REGISTER = "dokter/add"
    const val EDIT_PROFILE = "dokter/edit"
}