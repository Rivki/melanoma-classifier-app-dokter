package com.example.melanomaappfordoctor.data.network

import com.example.melanomaappfordoctor.data.network.request.LoginRequest
import com.example.melanomaappfordoctor.data.network.request.RegisterRequest
import com.example.melanomaappfordoctor.data.network.response.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.http.*

interface MainApi {

    @POST(ApiEndPoint.LOGIN)
    fun login(@Body request: LoginRequest): Observable<BaseResponse<LoginResponse>>

    @GET(ApiEndPoint.LIST_PERIKSA)
    fun getListPeriksa(): Observable<BaseResponse<ListPeriksaResponse>>

    @GET(ApiEndPoint.GET_DOKTER)
    fun getDokter(): Observable<BaseResponse<GetDokterResponse>>

    @GET(ApiEndPoint.GET_DETAIL_PERIKSA + "id={id}")
    fun getDetailPeriksa(@Path("id") id: Int): Observable<BaseResponse<DetailPeriksaResponse>>

    @Multipart
    @PUT(ApiEndPoint.EDIT_PERIKSA)
    fun editPeriksa(
        @Part("id_periksa") idPeriksa: RequestBody,
        @Part("kesimpulan") kesimpulan: RequestBody,
        @Part("deskripsi") deskripsi: RequestBody
    ): Observable<BaseResponse<EditPeriksaResponse>>

    @GET(ApiEndPoint.HISTORY_PERIKSA)
    fun historyPeriksa(): Observable<BaseResponse<HistoryPeriksaResponse>>

    @POST(ApiEndPoint.REGISTER)
    fun register(@Body request: RegisterRequest): Observable<BaseResponse<RegisterResponse>>

    @Multipart
    @PUT(ApiEndPoint.EDIT_PROFILE)
    fun editProfile(
        @Part imageFile: MultipartBody.Part?,
        @Part("nama") nama: RequestBody,
        @Part("no_telepon") noHp: RequestBody,
        @Part("jenis_kelamin") jenisKelamin: RequestBody,
        @Part("tempat_tanggal_lahir") tempatTanggalLahir: RequestBody,
        @Part("tanggal_lahir") tanggalLahir: RequestBody,
        @Part("umur") umur: RequestBody,
        @Part("alamat") alamat: RequestBody
    ): Observable<BaseResponse<EditProfileResponse>>

    companion object {
        fun create(retrofit: Retrofit): MainApi {
            return retrofit.create(MainApi::class.java)
        }
    }
}