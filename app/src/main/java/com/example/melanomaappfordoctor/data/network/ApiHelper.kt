package com.example.melanomaappfordoctor.data.network

import com.example.melanomaappfordoctor.data.network.request.EditPeriksaRequest
import com.example.melanomaappfordoctor.data.network.request.EditProfileRequest
import com.example.melanomaappfordoctor.data.network.request.LoginRequest
import com.example.melanomaappfordoctor.data.network.request.RegisterRequest
import com.example.melanomaappfordoctor.data.network.response.*
import io.reactivex.Observable

interface ApiHelper {
    fun login(loginRequest: LoginRequest): Observable<BaseResponse<LoginResponse>>

    fun listPeriksa(): Observable<BaseResponse<ListPeriksaResponse>>

    fun getDokter(): Observable<BaseResponse<GetDokterResponse>>

    fun getDetailPeriksa(id: Int): Observable<BaseResponse<DetailPeriksaResponse>>

    fun editPeriksa(request: EditPeriksaRequest): Observable<BaseResponse<EditPeriksaResponse>>

    fun historyPeriksa(): Observable<BaseResponse<HistoryPeriksaResponse>>

    fun register(request: RegisterRequest): Observable<BaseResponse<RegisterResponse>>

    fun editProfile(request: EditProfileRequest): Observable<BaseResponse<EditProfileResponse>>
}