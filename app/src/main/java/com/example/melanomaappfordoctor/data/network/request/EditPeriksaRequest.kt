package com.example.melanomaappfordoctor.data.network.request

import com.google.gson.annotations.SerializedName

class EditPeriksaRequest: BaseRequest() {
    @SerializedName("id_periksa")
    var idPeriksa: String? = null

    var kesimpulan: String? = null

    var deskripsi: String? = null
}