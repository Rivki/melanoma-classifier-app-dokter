package com.example.melanomaappfordoctor.data.network.response

import com.google.gson.annotations.SerializedName

data class ListPeriksaResponse(@SerializedName("data") val data: List<PeriksaResponse>)