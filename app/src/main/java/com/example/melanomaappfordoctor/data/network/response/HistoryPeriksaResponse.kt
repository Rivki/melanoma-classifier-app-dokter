package com.example.melanomaappfordoctor.data.network.response


import com.google.gson.annotations.SerializedName

data class HistoryPeriksaResponse(@SerializedName("data") val data: List<PeriksaResponse>)