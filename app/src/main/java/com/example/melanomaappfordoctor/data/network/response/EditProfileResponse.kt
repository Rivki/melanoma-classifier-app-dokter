package com.example.melanomaappfordoctor.data.network.response


import com.google.gson.annotations.SerializedName

data class EditProfileResponse(
    @SerializedName("alamat")
    val alamat: String,
    @SerializedName("create_at")
    val createAt: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("gambar")
    val gambar: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("jk")
    val jk: String,
    @SerializedName("modified_at")
    val modifiedAt: String,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("no_hp")
    val noHp: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("rate_dokter")
    val rateDokter: List<Any>,
    @SerializedName("tanggal_lahir")
    val tanggalLahir: String,
    @SerializedName("tempat_tanggal_lahir")
    val tempatTanggalLahir: String,
    @SerializedName("umur")
    val umur: Int
)