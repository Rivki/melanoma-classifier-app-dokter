package com.example.melanomaappfordoctor.data.preferences

interface PreferencesHelper {
    fun setToken(token: String)
    fun getToken(): String?
    fun resetAllPreferences()
    fun setIdUser(id: Int)
    fun getIdUser(): Int
}