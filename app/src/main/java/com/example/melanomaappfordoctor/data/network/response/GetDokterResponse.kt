package com.example.melanomaappfordoctor.data.network.response


import com.google.gson.annotations.SerializedName

data class GetDokterResponse(
    @SerializedName("alamat")
    val alamat: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("gambar")
    val gambar: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("no_hp")
    val noHp: String,
    @SerializedName("tanggal_lahir")
    val tanggalLahir: String,
    @SerializedName("tempat_tanggal_lahir")
    val tempatTanggalLahir: String,
    @SerializedName("umur")
    val umur: Int,
    @SerializedName("jk")
    val jk: String,
    @SerializedName("rate")
    val rate: Float
)