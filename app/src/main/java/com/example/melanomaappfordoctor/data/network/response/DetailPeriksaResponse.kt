package com.example.melanomaappfordoctor.data.network.response


import com.google.gson.annotations.SerializedName

data class DetailPeriksaResponse(
    @SerializedName("create_at")
    val createAt: String,
    @SerializedName("data_gambar_id")
    val dataGambarId: Int,
    @SerializedName("deskripsi")
    val deskripsi: Any,
    @SerializedName("dokter_id")
    val dokterId: Int,
    @SerializedName("email")
    val email: String,
    @SerializedName("gambar")
    val gambar: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("keluhan")
    val keluhan: String,
    @SerializedName("kesimpulan")
    val kesimpulan: Any,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("pasien_id")
    val pasienId: Int,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("hasil_prediksi")
    val hasilPrediksi: String,
    @SerializedName("umur")
    val umur: Int,
    @SerializedName("riwayat_penyakit")
    val riwayatPenyakit: String
)