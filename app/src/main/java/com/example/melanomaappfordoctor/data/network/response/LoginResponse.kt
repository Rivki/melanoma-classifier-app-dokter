package com.example.melanomaappfordoctor.data.network.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(@SerializedName("token") val token: String, val id: Int)