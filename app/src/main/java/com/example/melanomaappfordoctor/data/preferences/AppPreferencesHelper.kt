package com.example.melanomaappfordoctor.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.melanomaappfordoctor.di.PreferenceInfo
import javax.inject.Inject

class AppPreferencesHelper @Inject constructor(
    context: Context,
    @PreferenceInfo private val prefFileName: String
) : PreferencesHelper {

    private val KEY_USER_TOKEN = "KEY_USER_TOKEN"
    private val KEY_ID_USER = "KEY_ID_USER"

    private val mPrefs: SharedPreferences =
        context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    override fun setToken(token: String) {
        mPrefs.edit { putString(KEY_USER_TOKEN, token) }
    }

    override fun getToken(): String? = mPrefs.getString(KEY_USER_TOKEN,null)

    override fun resetAllPreferences() {
        val editor = mPrefs.edit()
        editor.clear()
        editor.apply()
    }

    override fun setIdUser(id: Int) {
        mPrefs.edit{putInt(KEY_ID_USER, id)}
    }

    override fun getIdUser(): Int = mPrefs.getInt(KEY_ID_USER, 0)

}