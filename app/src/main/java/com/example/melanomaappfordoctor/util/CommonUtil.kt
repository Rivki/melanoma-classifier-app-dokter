package com.example.melanomaappfordoctor.util

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.graphics.drawable.toDrawable
import com.example.melanomaappfordoctor.R
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

object CommonUtil {
    fun buildGson(): Gson {
        return GsonBuilder()
            .create()
    }

    fun showLoadingDialog(context: Context?): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        progressDialog.let {
            it.show()
            it.window?.setBackgroundDrawable(Color.TRANSPARENT.toDrawable())
            it.setContentView(R.layout.progress_dialog)
            it.isIndeterminate = true
            it.setCancelable(false)
            it.setCanceledOnTouchOutside(false)
            return it
        }
    }

    fun String.convertToPercent(): String{
        val text = this
        val convertDouble = text.toDouble()
        val convertToPercentage = convertDouble * 100
        val result =  convertToPercentage.toString().substring(0, 5)
        return result
    }

    @SuppressLint("SimpleDateFormat")
    fun String.formatDates(): String {
        val dates = this
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val inputFormat =
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS", Locale.getDefault())
            val outputFormat = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.getDefault())
            val date = LocalDate.parse(dates, inputFormat)
            return outputFormat.format(date)
        }else{
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
            val outputFormat = SimpleDateFormat("dd MMMM yyyy")
            val date = inputFormat.parse(dates)
            return outputFormat.format(date)
        }
    }
}